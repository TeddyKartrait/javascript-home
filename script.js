let arr = []
let openCard = []

document.addEventListener('DOMContentLoaded', createForm())

function createForm() {
    const container = document.getElementById('container'),
        form = document.createElement('form'),
        inp = document.createElement('input'),
        btn = document.createElement('button'),
        list = document.createElement('ul');

        inp.type = 'number';
        inp.id = 'inp';
        btn.classList.add('btn');
        list.classList.add('list')
        btn.textContent = "Начать игру";

        form.append(btn);
        form.append(inp);
        container.append(form);

        btn.addEventListener('click', (e) => {
            e.preventDefault()

            container.append(list)

            const num = document.getElementById('inp').value;

                if (num === "") {
                    return num
                } else {
                    const limit = Math.round(num * num / 2);

                    for (i = 1; i <= limit; i++) {
                        arr.push(i);
                        arr.push(i);
                    }

                    const arrNum = shuffle(arr)

                    for (const el of arrNum) {
                        const card = new AmazingCard(list, el)
                    }

                    btn.remove()
                    inp.remove()
                }
        })
}

function shuffle (array) {
    let m = array.length, t, i;

    while (m) {
        i = Math.floor(Math.random() * m--);
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }
    return array;
}

class Card {
    _cardNumber
    _open = false
    _success = false

    constructor(container, num) {
        this.item = this.createElement()
        this.container = container
        this.cardNumber = num

        
        this.item.addEventListener('click', () => {
            if(this.item.classList.contains('open')) {
                return
            } else {
                this.open = true
                this.item.classList.add('open')
                openCard.push(this)
            }
            
            if(openCard.length >= 2) {
                if(openCard[0]._cardNumber != openCard[1]._cardNumber) {
                    const checkCard = openCard
                    openCard = []
                    setTimeout(() => {
                        checkCard[0].open = false;
                        this.open = false
                    }, 750)
                } else {
                    openCard[0].success = true;
                    this.success = true
                    openCard = []
                }
            } 
        })

        container.append(this.item)
    }

    createElement() {
        const item = document.createElement('li')

        item.classList.add('card')

        return item
    }

    set cardNumber(value) {
        this._cardNumber = value

        if(this.item)
            this.item.textContent = value
    }

    get cardNumber() {
        return this._cardNumber
    }

    set open(value) {
        this._open = value
        if(value == true) {
            this.item.classList.add('open')
        } else {
            this.item.classList.remove('open')
        }
    }

    get open() {
        return this._open
    }

    set success(value) {
        this._success = value
        if(value == true)
            this.item.classList.add('success')
    }

    get success() {
        return this._success
    }
}

class AmazingCard extends Card {
    set cardNumber(value) {
        this._cardNumber = value

        const arrImg = [
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-1.png',
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-2.png',
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-3.png',
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-4.png',
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-5.png',
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-6.png',
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-7.png',
            'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-12.png',
        ]

        const img = document.createElement('img')

        img.classList.add('image')

        img.src = arrImg[value - 1]

        img.onerror = () => {
            img.src = 'C:/Users/jackk/OneDrive/Desktop/JavaScript/js-homework-9/js/img/img-default.png'
            throw new Error('Не удалось загрузить изображение')
        }

        this.item.append(img)
    }

    get cardNumber() {
        return this._cardNumber
    }
}